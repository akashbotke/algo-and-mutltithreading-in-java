package JavaMultiThread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HackerPoliceThreadCaseStudy {
    static int Max = 999;

    private static class Vault {
        int password;

        Vault(int password) {
            this.password = password;
        }

        private boolean isCorrectPassword(int guess) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return guess == this.password;
        }

    }

    private static abstract class HackerThread extends Thread {
        protected Vault vault;

        HackerThread(Vault vault) {
            this.vault = vault;
            this.setName(this.getClass().getSimpleName());
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void start() {
            System.out.println("Starting Method " + this.getName());
            super.start();
        }

    }

    private static class PoliceThread extends Thread {
        @Override
        public void run() {
            for(int i = 10; i > 0; i--) {
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            System.out.println("Game over for Hacker");
            System.exit(0);

        }
    }

    private static class AscendingHackerThread extends HackerThread {
        AscendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int guess = 1; guess <= Max; guess++) {
                if (vault.isCorrectPassword(guess)) {
                    System.out.println(this.getName() + " Guess the Password " + guess);
                    System.exit(0);
                }
            }
        }
    }

    private static class DescendingHackerThread extends HackerThread {
        DescendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int guess = Max; guess <= 1; guess--) {
                if (vault.isCorrectPassword(guess)) {
                    System.out.println(this.getName() + " Guess the Password " + guess);
                    System.exit(0);
                }
            }
        }
    }

        public static void main(String[] args) {

            Random random = new Random();
            Vault vault = new Vault(random.nextInt(Max));
            List<Thread> threadList = new ArrayList<>();
            threadList.add(new AscendingHackerThread(vault));
            threadList.add(new DescendingHackerThread(vault));
            threadList.add(new PoliceThread());

            for(Thread t : threadList)
            {
                t.start();
            }



        }
    }


