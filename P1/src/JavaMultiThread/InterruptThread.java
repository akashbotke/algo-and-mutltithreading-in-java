package JavaMultiThread;

import java.math.BigInteger;

public class InterruptThread {
    //two ways of interruption
    //interrupt cause the termination of thread

    public static void main(String[] args) {
        // 1. if we have thread which is in sleep or Blocking or in-active in mode in that case we need to stop it
        Thread thread = new BlockingTask();
        thread.start();
        thread.interrupt();

        //2. if we have thread which is computing/take to much time to execute calculation
        // we need to stop it

        Thread thread1 = new Thread(new ComputationTask(new BigInteger("200"),new BigInteger("1000")));
        thread1.setDaemon(true);
        thread1.start();
//        thread1.interrupt();



    }
   private static class BlockingTask extends Thread
    {
        @Override
        public void run()
        {
            try {
                Thread.sleep(50000);
            } catch (InterruptedException e) {
                System.out.println("Caught Exception");
//                throw new RuntimeException(e);
            }
        }


    }

    private static class ComputationTask implements Runnable
    {

        BigInteger base;
        BigInteger power;

        ComputationTask(BigInteger base,BigInteger power)
        {
            this.base=base;
            this.power=power;
        }
        @Override
        public void run() {
            System.out.println(base+ " to the power of "+power+" = "+pow());
        }

        public BigInteger pow()
        {
            BigInteger result = BigInteger.ONE;
            for(BigInteger i=BigInteger.ZERO;i.compareTo(power) != 0;i=i.add(BigInteger.ONE))
            {
                if(Thread.currentThread().isInterrupted())
                {
                    System.out.println("Interrupt Computation ");
                    return BigInteger.ZERO;
                }
                result = result.multiply(base);
            }

            return result;
        }
    }
}

