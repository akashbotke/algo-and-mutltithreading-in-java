package JavaMultiThread;

public class FristThread {
    // Threads are created two ways in java by extending Thread class and implementing Runnable Interface
    //Thread class has empty
    // we need to pass thread obj to it

    static class newThread extends Thread
    {
        @Override
        public void run() {
            //logic
            System.out.println("thread name is "+this.getName());
        }
    }
    public static void main(String args[]) {
        //creating thread by extending Thread class
        Thread t2 = new newThread();
        t2.start();
        //creating thread by implementing runnable interface and passing it in thread class
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("new thread with name " + Thread.currentThread().getName());
            }
        });

        // in order to run thread we need to start it

        System.out.println("Thread name before calling new thread "+Thread.currentThread().getName());
        thread.setName("New Thread");
        thread.start();
        System.out.println("Thread name after calling new thread "+Thread.currentThread().getName());

        // To handle exception in thread

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                throw new RuntimeException("Internal Exception");
            }
        });

        thread1.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Exception in thread "+t.getName()+" and Exception is "+e.getMessage());
            }
        });

        thread1.start();
    }
}

