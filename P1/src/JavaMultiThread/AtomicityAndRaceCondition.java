package JavaMultiThread;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicityAndRaceCondition {
    public static void main(String[] args) throws InterruptedException {

        //race condition means suppose we have 2 thread
        // performing same operation on
        // if one thread get access and other thread loss access and moved on
        //Atomicity allow to get access one by one to avoid race condition

        ShareCounter shareCounter = new ShareCounter();
        //thread1
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread 1 started");
                for(int i=0;i<50000;i++)
                {
                    shareCounter.increaseCounter();
                }
                System.out.println("Thread 1 end");
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread 2 started");
                for(int i=0;i<50000;i++)
                {
                    shareCounter.increaseCounter();

                }
                System.out.println("Thread 2 End");
            }
        }).start();


        Thread.sleep(2000);
//        thread1.join();
//        thread1.join();


        System.out.println( shareCounter.getAtomicCount());


    }


}

class ShareCounter{

//    int count;

    AtomicInteger atomicInteger = new AtomicInteger(0);


    public  void increaseCounter()
    {
//        count++;
        atomicInteger.incrementAndGet();
    }

    public int getAtomicCount() {
        return atomicInteger.get();
//        return count;
    }
}
