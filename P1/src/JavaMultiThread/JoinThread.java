package JavaMultiThread;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JoinThread {
    public static void main(String[] args) throws InterruptedException {

        List<Long> numbers = Arrays.asList(1L,23L,234L,3456L,456780L);
        List<FactorialThread> threads = new ArrayList<>();
        //create new thread for each number
        for(Long number : numbers)
        {
            threads.add(new FactorialThread(number));
        }

        //start thread for calculation
        for(Thread thread:threads)
        {
            thread.setDaemon(true);
            thread.start();
        }
        //without join method it main thread does not wait for all thread to complete task assing to them
        //join allows to wait main thread until all thread complete the task
        //we have apply join() for all threads
        for(Thread thread : threads)
        {
//            thread.join();
            //if pass time in join() it will wait for that time if any one of thread is not complete with
            //in time it not collect result for that
            //but we have to stop that long running thread
            //we can use Daemon thread concept
            thread.join(2000);
        }

        //print result

        for(int i=0;i<numbers.size();i++)
        {
            FactorialThread thread = threads.get(i);
            if(thread.isCompleted)
            {
                System.out.println("Factorial of "+numbers.get(i)+" = "+thread.getResult());
            }
            else {
                System.out.println(" calculation is pending "+numbers.get(i));
            }
        }
    }

    public static class FactorialThread extends Thread
    {
        Long number;
        boolean isCompleted=false;

        BigInteger result=BigInteger.ONE;

        FactorialThread(Long number)
        {
            this.number=number;
        }

        @Override
        public void run()
        {
            this.result=factorial(number);
            this.isCompleted=true;
        }

        public BigInteger factorial(long number)
        {
            BigInteger tempResult= BigInteger.ONE;
            for(long i=number;i>0;i--)
            {
                tempResult=tempResult.multiply(new BigInteger(Long.toString(i)));
            }

            return tempResult;
        }

        public boolean isCompleted() {
            return isCompleted;
        }

        public BigInteger getResult() {
            return result;
        }
    }
}
