package JavaMultiThread;/*
 * Copyright (c) 2019-2023. Michael Pogrebinsky - Top Developer Academy
 * https://topdeveloperacademy.com
 * All rights reserved
 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Optimizing for Latency Part 2 - Image Processing
 * https://www.udemy.com/java-multithreading-concurrency-performance-optimization
 */
public class ImageProcessing {
    public static final String SOURCE_FILE = "./resources/many-flowers.jpg";
    public static final String DESTINATION_FILE = "./out/many-flowers.jpg";

    public static void main(String[] args) throws IOException {

        BufferedImage originalImage = ImageIO.read(new File("./resources/many-flowers.jpg"));
        BufferedImage resultImage = new BufferedImage(originalImage.getWidth(),originalImage.getHeight(),BufferedImage.TYPE_INT_RGB);

        System.out.println(resultImage.getRaster());

        long startTime = System.currentTimeMillis();



        recolorMultithreaded(originalImage,resultImage,1);


        long endTime = System.currentTimeMillis();

        long duration = endTime - startTime;

        File outputFile = new File("./out/many-flowers-1.jpg");
        ImageIO.write(resultImage, "jpg", outputFile);

        System.out.println(String.valueOf(duration));
    }

    public static void recolorMultithreaded(BufferedImage originalImage, BufferedImage resultImage,int numberOfThread)  {
        int width = originalImage.getWidth();
        int height = originalImage.getHeight()/numberOfThread;

        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < numberOfThread; i++) {
            final int finalI = i;
            Thread thread = new Thread(() ->
            {
                int leftCorner=0;
                int topCorner= height* finalI;
                recolorImage(originalImage,resultImage,leftCorner,topCorner,width,height);

            });
            threads.add(thread);
        }

        for(Thread thread:threads)
        {
            thread.start();
        }
        for(Thread thread:threads)
        {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }


    public static void recolorSingleThreaded(BufferedImage originalImage, BufferedImage resultImage) {
        recolorImage(originalImage, resultImage, 0, 0, originalImage.getWidth(), originalImage.getHeight());
    }



    public static void recolorImage(BufferedImage originalImage,BufferedImage resulImage,int leftCorner,int topCorner,int width,int height)
    {

        for(int x=leftCorner;x<leftCorner+originalImage.getWidth() && x<originalImage.getWidth();x++)
        {
            for(int y=topCorner;y<topCorner+originalImage.getHeight() && y<originalImage.getHeight();y++)
            {
                changePixelRgb(originalImage,resulImage,x,y);
            }
        }
    }
    public static void changePixelRgb(BufferedImage originalImage,BufferedImage resultImage,int x,int y)
    {
        int rgb = originalImage.getRGB(x,y);
        int red=getRed(rgb);
        int blue = getBlue(rgb);
        int green = getGreen(rgb);

        if(isGrayColor(red,green,blue))
        {
            red = Math.min(255,red+10);
            green = Math.max(0,green-80);
            blue = Math.max(0,blue-20);
        }
//        return 0;

        int newRgb = getRGBPixelFromColors(red,blue,green);
        //setnewRGB pixel in image
        resultImage.getRaster().setDataElements(x,y,resultImage.getColorModel().getDataElements(newRgb,null));
    }
    public static boolean isGrayColor(int red,int green,int blue)
    {
        return Math.abs(red-green)<30 && Math.abs(green-blue)<30 && Math.abs(red-blue)<30;
    }
    public static int getRGBPixelFromColors(int red,int blue,int green)
    {
        int rgbPixel=0;

        rgbPixel |= blue;
        rgbPixel |= green << 8;
        rgbPixel |= red << 16;
        rgbPixel |= 0xFF000000;

        return rgbPixel;



    }

    public static int getRed(int rgb)
    {
        return (rgb & 0x00FF0000) >> 16;
    }
    public static int getBlue(int rgb)
    {
        return rgb & 0x000000FF;
    }
    public static int getGreen(int rgb)
    {
        return (rgb & 0x0000FF00) >> 8;
    }
}
