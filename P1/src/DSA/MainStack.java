package DSA;

public class MainStack {
    public static void main(String[] args) throws Exception {


        CustomStack stack = new DynamicStack(5);
        stack.push(10);
        stack.push(20);
        stack.push(2);
        stack.push(4);
        stack.push(3);
        stack.push(36);
        stack.print();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.push(90);
        stack.print();
        System.out.println( "\n Peek is "+stack.peek());




    }

}
