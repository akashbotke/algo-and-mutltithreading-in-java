package DSA;

import java.util.LinkedList;
import java.util.Hashtable;

public class HashTable {

    public static void main(String[] args) {

        LinkedList<String>[] hashTable = new LinkedList[5];
        String s="hi abc aa qs pl";

        String[] str_arr=s.split(" ");

        for(int i=0;i< str_arr.length;i++)
        { int hastcode = str_arr[i].hashCode();

          LinkedList<String> l1 = hashTable[hastcode%5];
          if(l1==null)
              l1=new LinkedList<>();
          l1.add(str_arr[i]);
          hashTable[hastcode%5]=l1;

        }

        for(int i=0;i<hashTable.length;i++)
        {
            LinkedList list = hashTable[i];
            if(list==null)
                continue;
            for(Object str:list)
            {
                System.out.print(str+" "+i+" ");
            }
            System.out.println();
        }
    }
}
