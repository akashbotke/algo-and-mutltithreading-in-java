package DSA;

public class DynamicStack extends CustomStack{

    public DynamicStack() {
        super();
    }

    public DynamicStack(int size) {
        super(size);
    }

    @Override
    public boolean push(int i) throws StackException {

        if(this.isFull())
        {
            int[] temp = new int[stack.length*2];
            //copy previous stack to new stack

            for (int j=0;j< stack.length;j++)
            {
                temp[j]=stack[j];
            }

            stack=temp;
        }

        return super.push(i);
    }
}
