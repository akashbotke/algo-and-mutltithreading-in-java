package DSA;

public class MainCyclicQueue {
    public static void main(String[] args) throws Exception{

        CyclicQueue queue = new CyclicQueue(5);
        queue.insert(3);
        queue.insert(13);
        queue.insert(32);
        queue.insert(4);
        queue.insert(56);
//        queue.insert(57);
        queue.display();

        System.out.println("removed "+queue.remove());
        queue.insert(58);
        System.out.println("removed "+queue.remove());
        queue.display();
        System.out.println("Size of queue is "+queue.size);







    }
}
