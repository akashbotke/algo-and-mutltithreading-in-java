package DSA;

import java.security.spec.ECFieldF2m;

public class CustomStack {

    protected int[] stack;

    private static final int DEFAULT_SIZE=10;
    private int pointer=-1;
    CustomStack()
    {
        this(DEFAULT_SIZE);
    }
    CustomStack(int size)
    {
        this.stack = new int[size];
    }




    public  boolean push(int i) throws StackException {

        if(isFull())
        {
            throw new StackException("Stack is Full!!");

        }

        pointer++;
        stack[pointer]=i;
        return true;

    }
    public  int pop() throws StackException {
        if(isEmpty())
            throw new StackException("Cannot use pop on Empty Stack!!");

        return stack[pointer--];
    }

    public boolean isFull() {
       return pointer == stack.length-1;
    }

    public boolean isEmpty()

    {
        return pointer==-1;
    }
    public  void print()
    {
        System.out.println("stack element \n");
        for(int i=0;i<=pointer;i++)
        {
            System.out.print(stack[i]+",");
        }
    }

    public int peek() throws StackException
    {
        if(isEmpty())
            throw new StackException("Cannot peek on Empty Stack!!");

        return stack[pointer];

    }
}

class StackException extends Exception
{

    public StackException(String message) {
        super(message);
    }
}
