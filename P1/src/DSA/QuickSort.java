package DSA;

import java.util.ArrayList;
import java.util.Random;

public class QuickSort {
    public static void main(String[] args) {

        Random random = new Random();
        int[] arr = new int[20];//{31,99, 43, 87, 8, 2, 95, 38, 5, 57 };//
        System.out.println("Before");
        for(int i=0;i<arr.length;i++)
        {
            arr[i]=random.nextInt(100);

            System.out.print(arr[i]+" ");
        }

        int low=0;
        int high=arr.length-1;
        quickSort(arr,low,high);
        System.out.println("\nafter");
        for (int j : arr) {
//            arr[i]=random.nextInt(100);
            System.out.print(j+" ");
        }


    }

    public static void quickSort(int[] arr, int low, int high)
    {

        if(low>=high)
            return;
        int pivot=arr[high];
        int lp=low;
        int rp=high;
        while(lp<rp)
        {

            while(arr[lp] <= pivot && lp < rp)
            {
                lp++;
            }

            while(arr[rp] >= pivot && lp<rp) {
                rp--;
            }

            swap(arr,lp,rp);
        }
        swap(arr,lp,high);
        quickSort(arr,low,lp-1);
        quickSort(arr,lp+1,high);
    }

    private static void swap(int[] arr, int lp, int rp) {

        int temp=arr[lp];
        arr[lp]=arr[rp];
        arr[rp]=temp;
//        System.out.println("swapped "+arr[lp]+" "+arr[rp]);

    }

//    Before
//31 99 43 87 8 2 95 38 5 57
//    after
//2 8 5 31 38 57 43 99 87 95


}

