package DSA;

public class CyclicQueue {

    protected int[] data;

    private static final int DEFAULT_SIZE=10;
    protected int end=0,front=0;
    int size=0;
    public CyclicQueue() {
        this(DEFAULT_SIZE);
    }
    public CyclicQueue(int n)
    {
        this.data= new int[n];
        this.size=0;
    }

    public boolean isFull() {
        return size == data.length;
    }

    public boolean isEmpty()

    {
        return size==0;
    }

    public void insert(int item)
    {
        if(isFull())
        {
            System.out.println("Queue is full");
            return;
        }

        data[end++]=item;
        end = end % data.length;
        size++;
        System.out.println("Added "+item+" size "+size);

    }

    public int remove() throws Exception
    {
        if(isEmpty())
            throw new Exception("Queue is Empty");
        int removed = data[front];
        front++;
        front = front % data.length;
        size--;

        return removed;
    }
    public void display()
    {
        int i=front;
        do{
            System.out.print(" "+data[i]);
            i++;
            i=i%data.length;
        }
        while(i!=end);
        System.out.println(" END ");
    }

}
