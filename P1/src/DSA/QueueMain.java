package DSA;

public class QueueMain {
    public static void main(String[] args) throws Exception{

        CustomQueue queue = new CustomQueue(5);
        queue.insert(3);
        queue.insert(13);
        queue.insert(32);
        queue.insert(4);
        queue.insert(56);
        queue.insert(57);
        queue.display();

        System.out.println(queue.remove());
        queue.insert(58);
        queue.display();




    }
}
