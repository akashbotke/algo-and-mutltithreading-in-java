package DSA.sorts;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {

        int arr[] ={99,4,1,5,88,66,77,4,45};

        //for quick sort we have select pivot
        //pivot is an element in array such that
        // left of that are small and right of that are bigger


        int left=0, right= arr.length-1;

        quickSort(arr,left,right);

        Arrays.stream(arr).forEach(System.out::println);

    }

    static void quickSort(int arr[],int left,int right)
    {
        if(left >= right)
            return ;
        int pivot = (left+right)/2;
        //search of element in  left side of pivot
        //if found and grater than pivot
        //we swap to right side of pivot with ele which is smaller than pivot
        int lp=left,rp=right;
        while(lp<rp)
        {

            while(lp< pivot)
            {
                if(arr[lp] > arr[pivot])
                {
                    System.out.println("found bigger than pivot "+arr[lp]+"for pivot "+arr[pivot]);
                    break;
                }
                lp++;
            }

            while(pivot<rp)
            {
                if(arr[rp] < arr[pivot])
                {
                    System.out.println("found smaller than pivot "+arr[rp]+"for pivot "+arr[pivot]);
                    break;
                }

                rp--;
            }

            swap(arr,lp,rp);
            lp++;
            rp--;

        }

        //left side sorting
        quickSort(arr,left,lp-1);
        //right side sorting
        quickSort(arr,lp,right);

    }

   static void swap(int[] arr,int lp,int rp)
    {
        System.out.println("swap index "+lp +" value "+arr[lp]+" and "+rp +" value "+arr[rp]);
        int temp=arr[lp];
        arr[lp]=arr[rp];
        arr[rp]=temp;
    }
}
