package Eeffective_Java_Book_Concept;

public class NutritionFacts_Builder_Pattern {

    // when we have many fields in class
    //and we want to create many constructor based on different requirement
    // so as fields are increasing it is difficult to maintain constructor and also difficult
    // to maintain sequence of fields in constructor

    //So we use Builder pattern, below are step for it
    //0. crate main class and add all fields including required and optional
    //1. crate Builder class with static keyword inside class
    //2. add all field as class parameter like required and optional fields
    //3. create Constructor in Builder class with only required fields
    //4.create setter like method for all other optional fields but it return Builder class
    //5.create build method that return main class with Builder class obj.
    //6. create Constructor of main class with parameter of Builder Obj and set all fields


    private final int servingSize;
    private final int servings;
    private final int fat;
    private final int calories;

    public static class Builder
    {
        //required
        private final int servingSize;
        private final int servings;

        //optional
        private  int fat;
        private  int calories;

        Builder(int servingSize,int servings)
        {
            this.servingSize=servingSize;
            this.servings=servings;
        }

        public Builder calories(int calories)
        {
            this.calories=calories;
            return this;
        }

        public Builder fat(int fat)
        {
            this.fat=fat;
            return this;
        }

        public NutritionFacts_Builder_Pattern build()
        {
            return new NutritionFacts_Builder_Pattern(this);
        }


    }

    public int getServingSize() {
        return servingSize;
    }

    public int getServings() {
        return servings;
    }

    public int getFat() {
        return fat;
    }

    public int getCalories() {
        return calories;
    }


    private NutritionFacts_Builder_Pattern(Builder builder)
    {
        servingSize= builder.servingSize;
        servings = builder.servings;
        calories= builder.calories;
        fat=builder.fat;
    }

    public static void main(String[] args) {

        NutritionFacts_Builder_Pattern chips = new NutritionFacts_Builder_Pattern.Builder(240,8).calories(60).fat(10).build();
        System.out.println(chips.getFat());
    }

}
