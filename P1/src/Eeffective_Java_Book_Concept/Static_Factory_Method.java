package Eeffective_Java_Book_Concept;

public class Static_Factory_Method {


    //Benefits of having static factory method can be use instead of Constructor
    //1.Construction don't have meaningful names
    //2.Static factory can return sub-types
    //3.static fatory can encapsulate all logic required to create  it
    //4.static factory can control instance method like Singleton pattern

    // they are different name are used for static factory method
    //like getInstance(),getObject(),of(),getType(),valueOf(),from(),type(),newType()

    private int rollNo;
    private String name;

    private String location;

    public Static_Factory_Method(int rollNo, String name, String location) {
        this.rollNo = rollNo;
        this.name = name;
        this.location = location;
    }

    public static Static_Factory_Method getPuneStudent(int rollNo, String name)
    {

        return new Static_Factory_Method(rollNo,name,"Pune");

    }

    public static Static_Factory_Method getMumbaiStudent(int rollNo, String name)
    {

        return new Static_Factory_Method(rollNo,name,"Mumbai");

    }

    @Override
    public String toString() {
        return "Static_Factory_Method{" +
                "rollNo=" + rollNo +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    public static void main(String[] args) {

        // with static factory method we are able to create two different obj with diff filed value
        //with using any setter method and without using multiple constructor

        Static_Factory_Method pune = Static_Factory_Method.getPuneStudent(1,"akash");
        Static_Factory_Method mumbai = Static_Factory_Method.getMumbaiStudent(2,"asd");

        System.out.println(pune.toString());
        System.out.println(mumbai.toString());


    }
}
