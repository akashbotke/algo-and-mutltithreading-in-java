package Eeffective_Java_Book_Concept;

public class Singleton {

    private String string;
    private static Singleton singleton;

    private Singleton()
    {
        string="Hi all I am intance variable of Singleton class";
    }

    public static  Singleton getInstance()
    {
        if(null==singleton)
            singleton = new Singleton();
        return singleton;
    }

    public static void main(String[] args) {

        Singleton singleton1 = Singleton.getInstance();

        Singleton singleton2 = Singleton.getInstance();

        singleton1.string=singleton1.string.toUpperCase();
        System.out.println(singleton1.string);
        System.out.println(singleton2.string);

        singleton2.string =singleton2.string.toLowerCase();
        System.out.println(singleton1.string);
        System.out.println(singleton2.string);

    }
}
