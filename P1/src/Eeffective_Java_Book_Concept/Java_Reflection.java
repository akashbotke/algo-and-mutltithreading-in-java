package Eeffective_Java_Book_Concept;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Java_Reflection {

    public static void main(String[] args) throws ClassNotFoundException {
        //Reflection api are used to examine or modify the behavior of methods,classes and inteface at runtime
        // Reflection gives us information about the class to which an object belongs and also the methods of that class that can be executed by using the object.
        //Through reflection, we can invoke methods at runtime irrespective of the access specifier used with them.

        // Below EMP class we can get all info of that class using Reflection api

        Class<?> c = Class.forName("Eeffective_Java_Book_Concept.Emp");

        Constructor<?>[] constructors = c.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println("Constructor name is " + constructor.getName());
            System.out.println("Number of parameter in Constructor " + constructor.getParameterCount());
            Parameter[] parameters = constructor.getParameters();

            for (Parameter parameter : parameters) {
                System.out.println("Parameter of above Constructor " + parameter);
            }

        }

        //Method array
        Method[] declaredMethods = c.getDeclaredMethods();
        System.out.println("Length of method : " + declaredMethods.length);

        for (Method method : declaredMethods) {
            System.out.println("Method name: \t" + method);
            System.out.println("Method return type : \t" + method.getReturnType());
            System.out.println("Method parameter count: \t" + method.getParameterCount());
            System.out.println();
            Parameter[] parameters = method.getParameters();
            for (Parameter parameter : parameters) {
                System.out.println("Method's Parameter : " + parameter);
            }
            System.out.println();

            //       classes in side
            Class[] classes = c.getDeclaredClasses();
            for (Class class1 : classes) {
                System.out.println("class: " + class1);
                System.out.println("Name of class: " + class1.getName());
            }
//        Annotations
            Annotation[] anno = c.getDeclaredAnnotations();
            for (Annotation annotation : anno) {
                System.out.println("Annotation: " + annotation);
            }

        }
    }

}
class Emp{
    private int eid;
    private double esal;
    private String ename;

    enum Week{
        SUN,TUE,WED;
    }

    @interface MyAnno{}

    public int getEid(){
        return eid;
    }
    public void setEid(int eid,int num, char ch){
        this.eid = eid;
    }

    public double getEsal(){
        return esal;
    }
    public void setEsal(double esal,float data, String name){
        this.esal = esal;
    }

    public String getEname(){
        return ename;
    }
    public void setEname(String ename){
        this.ename = ename;
    }

    // constructor
    public Emp(int eid, double esal, String ename){
        super();
        this.eid = eid;
        this.esal = esal;
        this.ename = ename;
    }
    Emp(){
    }
    class A{
    }
    class B{
    }
}

