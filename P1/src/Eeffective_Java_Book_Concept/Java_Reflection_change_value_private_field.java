package Eeffective_Java_Book_Concept;

import java.lang.reflect.Field;

public class Java_Reflection_change_value_private_field {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Company company = new Company(1,"facebook");

        System.out.println(company);
        Field field = company.getClass().getDeclaredField("company_name");
        field.setAccessible(true);
        field.set(company,"meta");
        System.out.println(company);


    }
}

class Company
{
    private int company_id;
    private String company_name;

    public Company(int company_id, String company_name) {
        this.company_id = company_id;
        this.company_name = company_name;
    }

    @Override
    public String toString() {
        return "Company{" +
                "company_id=" + company_id +
                ", company_name='" + company_name + '\'' +
                '}';
    }
}
