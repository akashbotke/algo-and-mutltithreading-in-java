package DSA_Problem;

public class Xor_Find_single_ele_in_arr {
    public static void main(String[] args) {

        // array contain duplicate ele find ele which is single

//       1 ^ 1=0
//       0 ^ 1=1
//       1 ^ 0=1
//       0 ^ 0=0
        int[] arr=new int[]{1,2,3,2,3,1,4};
        int res=0;
        for(int i=0;i<arr.length;i++)
        {
            res=res^arr[i];
        }

        System.out.println(res);
        System.out.println(arr[1]^arr[1]);

    }
}
