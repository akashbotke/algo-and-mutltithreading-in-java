package DSA_Problem;

public class FindCommonEleInSortedArray {
    public static void main(String[] args) {


        int arr1[]=new int[]{1,3,4,5,6,7};
        int arr2[]=new int[]{3,4,7,8,9,10};

        int a1=0;
        int a2=0;
        // n complexity
        while(a1<arr1.length)
        {
            if(arr1[a1]<arr2[a2])
            {
                a1++;
            }
            if(arr1[a1]==arr2[a2])
            {
                System.out.println(arr1[a1]);
                a1++;
                a2++;
            }
        }

        // below take n^2 time complexity
        int count=0;
        for(int i=0;i<arr1.length;i++)
        {
            for(int j=0;i<arr2.length;j++)
            {
                if(arr1[i]==arr2[j])
                {
                    System.out.println(arr1[i]);
                    break;
                }
                if(arr1[i] < arr2[j])
                {
                    break;
                }
            }
        }



    }
}
