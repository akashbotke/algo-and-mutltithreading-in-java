package DSA_Problem.string;

public class URLify {
    public static void main(String[] args) {
        String s="Mr John Smith     ";
        int original_length=13;
        //we can use replace method as it create new String
        //first we calculate blank space
        int blank_space=0;
        for (int i = 0; i < original_length; i++) {
            if(s.charAt(i)==' ')
                blank_space++;

        }
        int total_length=original_length+blank_space*2;
        char s_chr[]=s.toCharArray();
        for (int i = original_length-1; i >= 0 ; i--) {

            if(s.charAt(i)!=' ')
                s_chr[--total_length]=s_chr[i];
            else {
                s_chr[total_length-1]='0';
                s_chr[total_length -2]='2';
                s_chr[total_length -3]='%';
                total_length-=3;
            }

        }

        System.out.println(new String(s_chr));
    }
}
