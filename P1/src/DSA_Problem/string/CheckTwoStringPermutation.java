package DSA_Problem.string;

public class CheckTwoStringPermutation {
    public static void main(String[] args) {

        String s1 ="tacocat";
        String s2="tacotar";
        //permutation means both str has same number of char
        //S1:- we are sorted it then compare
        //this is not efficient
        //S2:- we count of char in str1 and then compare with count of str2

        //S2:-

        int count_chr[]=new int[128];
        //used to store only ASCII string
        //for unicode we have to use 256 char array

        //count char in str1
        char[] s1_char = s1.toCharArray();
        for(char c:s1_char)
            count_chr[c]++;
        //check count of char in str2

        for(int i=0;i<s2.length();i++)
        {
         count_chr[s2.charAt(i)]--;
         if(count_chr[s2.charAt(i)]<0)
             System.out.println("not permutation");

        }



    }
}
