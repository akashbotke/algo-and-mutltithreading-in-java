package DSA_Problem.string;

import java.util.Arrays;

public class CheckStringHasUniqueChar {
    public static void main(String[] args) {

        //suppose string is of ASCII type means it has only 128 character
        //for unicode String it can has 256 character in it

        //Solution1
        //S1:-to check unique char in str if we compare str with itself it get O(N^2)
        //S2:-sorted the char of string and check adjacent ele is same or not
        //S3:- create array of 128 size and store true and false true for first time and second time
        //if found true then return false otherwise true
        //implement S1:-
        String str="abcdef";
        System.out.println(S1(str));
        System.out.println(S2(str));
        System.out.println(S3(str));

    }

    public static boolean S1(String str)
    {
        for(int i=0;i<str.length();i++)
        {
            for(int j=i+1;j<str.length();j++)
            {
                if(str.charAt(i)==str.charAt(j))
                    return false;
            }
        }
        return true;
    }

    public static boolean S2(String str)
    {
        char[] str_chr = str.toCharArray();
        Arrays.sort(str_chr);
        for(int i=0;i< str_chr.length-2;i++)
        {
            if(str_chr[i]==str_chr[i+1])
                return false;
        }

        return true;

    }


    public static boolean S3(String str) {
       boolean[] char_set=new boolean[128];
        for (int i = 0; i < str.length(); i++) {
            if(char_set[(int)str.charAt(i)])
                return false;

            char_set[(int)str.charAt(i)]=true;
        }

        return true;
    }
}
