package DSA_Problem.string;

public class PalindromePermutation {
    public static void main(String[] args) {

        String s="tact coaoam";
        int[] char_count=new int[26];

        for(int i=0;i<s.length();i++)
        {
            //this check is used to check char is between a and z
            if(Character.getNumericValue('a') <= Character.getNumericValue(s.charAt(i)) && Character.getNumericValue('z')>=Character.getNumericValue(s.charAt(i)))
            {
                char_count[Character.getNumericValue(s.charAt(i))-Character.getNumericValue('a')]++;

            }
        }
        int cnt=0;
        for(int i=0;i<26;i++)
        {
            if(char_count[i]%2==1)
            {
             cnt++;
            }
        }

        if(cnt==1 || cnt==0)
            System.out.println("palindrome");
        else
            System.out.println("not a palindrome");
    }
}
