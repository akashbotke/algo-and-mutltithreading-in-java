package DSA_Problem.string;

import java.util.Arrays;

public class FIndSmallerStrPermutationInBiggerString {

    public static void main(String[] args) {
        String small="abbc";
        String big  = "cbabadcbbabbcbabaabccbabc";

        for(int i=0;i<big.length();i++)
        {
            if(i<=big.length()-small.length())
            {
                String subString = big.substring(i,i+small.length());
                //check subString is permutation
                if(isPermutation(small,subString))
                {
                    System.out.println("Str "+subString+" Start index "+i+" last index "+(i+small.length()));
                }
            }
        }
    }

    public static boolean isPermutation(String a,String subString)
    {

        char[] a_char =a.toCharArray();
        char[] b_char = subString.toCharArray();
        Arrays.sort(a_char);
        Arrays.sort(b_char);

        // Compare sorted strings
        for (int i = 0; i < a.length(); i++)
            if (a_char[i]!=b_char[i] )
                return false;

        return true;
    }
}
