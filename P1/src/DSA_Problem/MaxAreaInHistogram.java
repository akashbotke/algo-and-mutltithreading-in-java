package DSA_Problem;

import java.util.Stack;

public class MaxAreaInHistogram {

    public static void main(String[] args) {

        int area=maxArea(new int[]{2,2,5,6,3,3});
        System.out.println(area);
    }


    public static int maxArea(int[] arr)
    {
        Stack<Integer> stack = new Stack<>();
        int first=arr[0];
        stack.push(first);
        int right=0;
        int left=0;
        int max=arr[0];
        for(int i=0;i<arr.length;i++)
        {
            //right
            right=0;
            left=0;
           for(int j=i+1;j<arr.length;j++)
           {
               if(arr[j]>=arr[i])
               {
                   right++;
               }else{
                   break;
               }
           }
           //left
           for(int k=i-1;k>=0;k--)
           {
               if(arr[k]>=arr[i])
               {
                   left++;
               }
               else {
                   break;
               }
           }

           max = Math.max(max,((right+left+1)*arr[i]));
           System.out.println("i "+i+" right "+right+" left "+left+" "+ " max "+max);

        }

        return max;
    }
}
