// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
import java.util.HashMap;
public class Main {
    public static void main(String[] args) {

        int[] arr={9,4,5,7,1};
        //Insertion sort

        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<arr.length-1-i;j++)
            {
                if(arr[j] > arr[j+1])
                {
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
        }
        System.out.println("Bubble sort ");
        for(int x:arr)
        {
            System.out.print(x+" ");
        }
        System.out.println("Insertion sort ");
        for(int i=1;i<arr.length;i++)
        {
            int j=i-1;
            int x=arr[i];
//            System.out.println("arr[i] "+arr[i]);
            while(j>-1 && arr[j]<x)
            {
                arr[j+1]=arr[j];
                j--;
            }
//            System.out.println("arr[i] "+arr[i]);
            arr[j+1]=x;
        }
    for(int x:arr)
    {
        System.out.print(x+" ");
    }
    }
}