package Java8_practice;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamCreation {



    public static void main(String[] args) {

        // Create Stream 
        //empty stream
        Stream<Object> empty = Stream.empty();
        //array
        int arr[]={1,2,3,4,5};
        IntStream stream1 = Arrays.stream(arr);
        //using of
        Stream<int[]> arr1 = Stream.of(arr);
        Stream<Integer> integerStream1 = Stream.of(1, 2, 3, 4);

        //how create stream
        //
        List<Employee> employees = new ArrayList<>();
        employees.add( new Employee("Ajay",35,"a",true,1003));
        employees.add(new Employee("Vijay",5,"b",false,3345));
        employees.add( new Employee("Zack",30,"a",true,3456));
        employees.add(new Employee("David",51,"b",false,4566));
        employees.add(new Employee("David",52,"a",true,1234));

        //create Stream from list
        Stream<Employee> stream = employees.stream();
        stream.filter(x->x.firstName.startsWith("A")).forEach(System.out::println);
        //convert list to array
        Employee[] array = employees.stream().toArray(Employee[]::new);

        //convert list to map
        //this does allow us update keys values if encounter same key again in list
//        Map<String, Integer> collect = employees.stream().collect(Collectors.toMap(employee -> employee.firstName, employee -> employee.age));

        //this allow to add updated key value with updated/latest value for key
        //mergeFunction will resolve the duplicate key issue. mergeFunction is defined as a binary operator used to resolve the collisions between values associated with the same key.
        Map<String, Integer> collect1 = employees.stream().collect(Collectors.toMap(employee -> employee.firstName, employee -> employee.age,(integer, integer2) -> integer2));

        for(Map.Entry<String,Integer> map:collect1.entrySet())
            System.out.println(map.getKey() + "-" + map.getValue());

        //Collectors.toCollection()
        //As we’ve already noted, when using the toSet and toList collectors, we can’t make any assumptions of their implementations.
        // If we want to use a custom implementation,
        // we’ll need to use the toCollection collector with a provided collection of our choice.

        LinkedList<Employee> collect2 = employees.stream().collect(Collectors.toCollection(LinkedList::new));

        //Function.identity() is just a shortcut for defining a function that accepts and returns the same value.
// Here key is whole object
        Map<String,Integer> collect3 = employees.stream().collect(Collectors.toMap(Employee::getFirstName, employee->employee.age,(old,latest)->latest));


        //sorting
        employees.stream().sorted((e1,e2)->e1.firstName.compareTo(e2.firstName)).forEach(System.out::println);
        //mutliple comparing
        employees.stream().sorted((e1,e2)->e1.firstName.compareTo(e2.firstName)).forEach(System.out::println);

        //old way comparing
Comparator sortByName = new Comparator<Employee>() {
    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.firstName.compareTo(o2.firstName);
    }


};

        Comparator sortByage = new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.age-o2.age;
            }


        };
    System.out.println("Sorted by name and age");
        employees.stream().sorted(sortByName).sorted(sortByage).forEach(System.out::println);

        // using java 8
        // sorting on multiple fields
        employees.stream().sorted(Comparator.comparing(Employee::getFirstName).thenComparing(Employee::getDept,Comparator.reverseOrder()))

                .forEach(x->System.out.println(x));


        ///IMp Question

        //1. get list of emp in each dept

        Map<String,List<Employee>> list1= employees.stream().collect(Collectors.groupingBy(Employee::getDept,Collectors.toList()));
    list1.entrySet().forEach(e->{
        System.out.println(e.getKey()+"--"+e.getValue());
 });
        //2. emp count in each dept

        Map<String,Long> deptEmpCount = employees.stream().collect(Collectors.groupingBy(Employee::getDept,Collectors.counting()));

        deptEmpCount.entrySet().forEach(emp->{
            System.out.println(emp.getKey()+"-"+emp.getValue());
        });

        //3. print active and in-active emp

        Map<Boolean, List<Employee>> collect = employees.stream().collect(Collectors.partitioningBy(Employee::isActive));

        collect.entrySet().forEach(entry->{
            System.out.println(entry.getKey()+" "+entry.getValue().size());
        });

        // get min and max salary

        Optional<Employee> max = employees.stream().max(Comparator.comparing(Employee::getSalary));
        System.out.println(max.get());
        //using intToMap()
        OptionalInt max2 = employees.stream().map(e -> e.getSalary()).mapToInt(Integer::intValue).max();
        System.out.println("max "+max2.getAsInt());


        //get max salary in each dept wise

        Map<String, List<Employee>> collect4 = employees.stream().collect(Collectors.groupingBy(Employee::getDept, Collectors.toList()));

        collect4.entrySet().forEach(entry->
        {
            Optional<Employee> max1 = entry.getValue().stream().max(Comparator.comparing(Employee::getSalary));
           // max1.isPresent()
            System.out.println("Dept "+entry.getKey()+"-"+max1.get().getSalary());

        });

        //other by using reducing

        Map<String, Optional<Employee>> collect5 = employees.stream().collect(Collectors.groupingBy(Employee::getDept, Collectors.reducing(BinaryOperator.maxBy(Comparator.comparing(Employee::getSalary)))));
        collect5.entrySet().forEach(entry->{
            System.out.println(entry.getKey()+"-"+entry.getValue().get());
        });

        //count char in string

        String s="javajavaee";


        Map<String,Long> res=Arrays.stream(s.split("")).collect(Collectors.groupingBy(s1->s1,Collectors.counting()));
        Map<String, Integer> collect6 = Arrays.stream(s.split("")).collect(Collectors.toMap(s4->s4/* Function.identity()*/, s3 -> 1, (old, latest) -> old + 1));

        res.entrySet().forEach(entry->{
            System.out.println(entry.getKey()+"-"+entry.getValue());
        });
        collect6.entrySet().forEach(entry->{
            System.out.println(entry.getKey()+"-"+entry.getValue());
        });

        //mapping
        //groupingBy() discussed in the section above, groups elements of the stream with the use of a Map.
        //
        //However, sometimes we might need to group data into a type other than the element type.
        //
        //Here’s how we can do that; we can use mapping() which can actually adapt the collector to a different type – using a mapping function:
        //
        //@Test
        //public void whenStreamMapping_thenGetMap() {
        //    Map<Character, List<Integer>> idGroupedByAlphabet = empList.stream().collect(
        //      Collectors.groupingBy(e -> new Character(e.getName().charAt(0)),
        //        Collectors.mapping(Employee::getId, Collectors.toList())));
        //    assertEquals(idGroupedByAlphabet.get('B').get(0), new Integer(2));
        //    assertEquals(idGroupedByAlphabet.get('J').get(0), new Integer(1));
        //    assertEquals(idGroupedByAlphabet.get('M').get(0), new Integer(3));
        //}

    //IntStream,DoubleStream,longStream
    // function are range(a,b)-start with a and end b-1,sum(),sorted(),rangeClosed(a,b)-start with a and increase by 1 until b
    // min,max,average,filter,reduce,map,forEach,IntStream.of()
    // convert IntStream into int[] ex:-  int[] arr1 = IntStream.of(1,2,3,4,5).toArray();
        //IntStream mapToObj() operation
        //IntStream has a method called mapToObj that takes a function that maps an int to an Integer Object.
        //
        //IntStream.of(1, 2, 3, 4, 5).mapToObj(i -> {
        //  return Integer.toString(i * i);
        //}).forEach(System.out::println);

//this stream for primitive datatypes
        //therefore Stream<Integer> is different than intStream

        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        int sum = integerStream.mapToInt(Integer::intValue).sum();
        System.out.println(sum);



        //how to generate infinite stream
        //generate and iterate() are used to create infinite stream
        // skip() and limit() are used to get finite stream
        //skip() it skip line ex:- skip(3) it skip 3 ele
        //limit() it use to get finite stream ex:- limit(10) return 10 ele


        Stream.generate(Math::random).limit(5)
                .map(x->x*100)
                .mapToInt(Double::intValue)
                .forEach(System.out::println);

        //iterate
        //iterate() takes two parameters: an initial value, called seed element and a function which generates next element using the previous value.
        // iterate(), by design, is stateful and hence may not be useful in parallel streams:

        Stream.iterate(2,i->i*2).limit(10).forEach(System.out::println);


        //convert array int to list of Integer
        //we use    boxing

        int num[]={1,23,45,66};
        List<Integer> collect7 = Arrays.stream(num).boxed().toList();

        //Object to primitive
        List<Integer> l1 = new ArrayList<>();
        l1.add(1);l1.add(2);
        // we can convert Integer to int
        //object to primitive type
        int[] array1 = l1.stream().mapToInt(Integer::intValue).toArray();

        //Optional class
        //How to check if list is empty in Java 8 using Optional,
        // if not null iterate through the list and print the object?
        Optional.ofNullable(l1).orElseGet(Collections::emptyList)
        .stream()
                .filter(Objects::nonNull)
                .forEach(System.out::println);


        //checking frequency
        String str_arr[]={"aa","bb","aa","ac"};

        Arrays.stream(str_arr).filter(x->Collections.frequency(Arrays.asList(str_arr),x)>1)
                .collect(Collectors.groupingBy(str6->str6,Collectors.counting()));





    }

}
class Employee {
    String firstName;
    int age;

    String dept;

    boolean active;

    int salary;
    Employee(String name,int age,String dept,boolean active,int salary)
    {
        this.firstName=name;
        this.age=age;
        this.dept=dept;
        this.active=active;
        this.salary=salary;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", age=" + age +
                ", dept='" + dept + '\'' +
                ", active=" + active +
                ", salary=" + salary +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public int getAge() {
        return age;
    }

    public String getDept() {
        return dept;
    }

    public boolean isActive() {
        return active;
    }
}
