package Java8_practice;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GetCharCountWithStream {

    public static void main(String[] args) {

        String s="JavaJavaee";
        Map<String, Long> collect = Arrays.stream(s.split("")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for(Map.Entry<String,Long> entry:collect.entrySet())
        {
            System.out.println(entry.getKey()+"-"+entry.getValue());
        }

    }
}
