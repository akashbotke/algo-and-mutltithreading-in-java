package Java8_practice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateAndTimeNewApi8 {
    public static void main(String[] args) throws ParseException {

        //get current date
        LocalDate date = LocalDate.now();
        System.out.println(date);//represent Date in YYYY-MM-DD
        //get current time
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println(dateTime);



        //convert  Date to LocalDate
        Date date1 = new Date();
        System.out.println(date1.toInstant());
        //localDate require timezone to we have add additionally
        LocalDate localDate = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        //convert localdate to date
        Date date2 = java.sql.Date.valueOf(localDate);
        System.out.println(date2);

        //LocalDate to sql date
        LocalDate locald = LocalDate.of(1967, 06, 22);
        java.sql.Date sqlDate = java.sql.Date.valueOf(locald);
        //reverser from sql Date to Local Date
        LocalDate localD = sqlDate.toLocalDate();
        //util.Date has timeStamp but sql.Date don't have timeStamp

        //convet sql.date to util.date
        java.util.Date  utilDate = new java.util.Date(sqlDate.getTime());
        //convert util date to sql date
        java.util.Date utilDate1= new java.util.Date();
        java.sql.Date sqlDate1 = new java.sql.Date(utilDate1.getTime());

        //Convert String date to util.Date
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("dd-MM-yyyy");
        Date parse = simpleDateFormat.parse("22-02-2024");//throws parse Exception
        System.out.println("String date to util.Date"+ parse);

        //to conver date into string
        String format = simpleDateFormat.format(parse);
        System.out.println("change Date to String format"+format);

        //convert String to LocalDate
        //// BASIC_ISO_DATE formatter can parse date in yyyyMMdd format
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.BASIC_ISO_DATE;
        LocalDate localDate1 = LocalDate.parse("20240222",dateTimeFormatter);
        System.out.println(localDate1);

        //change LocalDate to String
        String format1 = localDate1.format(dateTimeFormatter);
        System.out.println("LocalDate to String "+format1);

        //to have diff String format

        dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        date = LocalDate.parse("27/09/2015", dateTimeFormatter);
        dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate parse1 = LocalDate.parse("27-11-1996",dateTimeFormatter);
        System.out.println(parse1);


//        String format1 = date.format(format);
//       System.out.println(format1);


    }
}
